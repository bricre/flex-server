FROM ubuntu as code
WORKDIR /var/www/html
RUN apt update && apt install -y wget git && \
    wget https://get.symfony.com/cli/installer -O - | bash && \
    git clone https://github.com/moay/server-for-symfony-flex .

FROM composer:1 as composer
WORKDIR /var/www/html
COPY --from=code /var/www/html .
RUN composer --no-dev install 

FROM node as node
WORKDIR /var/www/html
COPY --from=composer /var/www/html .
RUN npm install && yarn encore production

FROM php:7.2-alpine
RUN apk add git 
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" 

WORKDIR /var/www/html
COPY --from=node /var/www/html .
COPY --from=code /root/.symfony/bin/symfony /usr/local/bin/symfony
ADD entrypoint.sh /var/www/html/
ENV APP_ENV=prod
EXPOSE 8000
CMD ["/var/www/html/entrypoint.sh"]