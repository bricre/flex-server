# Ready to run Docker image of [server-for-symfony-flex](https://github.com/moay/server-for-symfony-flex)


```sh
docker run -p "8000:8000" bricre/symfony-flex-server
```

To have private Symfony Recipes available, set up environmental variable `FLEX_RECIPE_REPO_PRIVATE`

```sh
docker run -p "8000:8000" -e FLEX_RECIPE_REPO_PRIVATE=https://github.com/your-recipe-repo bricre/symfony-flex-server
```
